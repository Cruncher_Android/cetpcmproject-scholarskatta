import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotesChaptersPage } from './notes-chapters.page';

describe('NotesChaptersPage', () => {
  let component: NotesChaptersPage;
  let fixture: ComponentFixture<NotesChaptersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesChaptersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotesChaptersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
