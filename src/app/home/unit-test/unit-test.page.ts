import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SubjectsService } from './../../services/subjects.service';
import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-unit-test',
  templateUrl: './unit-test.page.html',
  styleUrls: ['./unit-test.page.scss'],
})
export class UnitTestPage implements OnInit {

  subjects: {subjectId: null,
              subjectName: '',
              imagePath: ''}[] = [];
  constructor(private inAppBrowser: InAppBrowser,
              private subjectsService: SubjectsService,
              private storage: Storage,
              private router: Router,
              private platform: Platform) {
    // console.log('unit-test page loaded');
  }

  ngOnInit() {
    // console.log(this.router.url);
    this.storage.set('page', 'unit-test');
    this.subjectsService.loadSubjects().then( subjects => {
      this.subjects = subjects;
      // console.log('subjects', this.subjects)
    });
  }
  openInBrowser() {
    this.inAppBrowser.create('https://ionicframework.com/docs/native/in-app-browser');
  }

}
