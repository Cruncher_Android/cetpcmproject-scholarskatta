import { PopoverController, LoadingController } from "@ionic/angular";
// import { ViewTestPage } from './../home/view-test/view-test.page';
// import { SaveTestInfoService } from './../services/save-test-info.service';

import { Storage } from "@ionic/storage";
import { Router, RouterEvent } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { MenuController, Platform, AlertController } from "@ionic/angular";
import { LoaderComponent } from "../components/loader/loader.component";
import { DatabaseServiceService } from "../services/database-service.service";
import { LoadingServiceService } from "./../services/loading-service.service";
import { UserInfoService } from "./../services/user-info.service";
import { HttpClient } from "@angular/common/http";
import { UserInfo } from "./../main/main.page";
import { StorageServiceService } from "./../services/storage-service.service";
import { InternetServiceService } from "./../services/internet-service.service";
import { AlertServiceService } from "./../services/alert-service.service";
import { ApiServiceService } from "./../services/api-service.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"],
})
export class DashboardPage implements OnInit {
  pages = [
    {
      title: "PROFILE",
      url: "/dashboard/profile",
      icon: "person",
    },
    {
      title: "HOME",
      url: "/dashboard/home",
      icon: "home",
    },
    {
      title: "TEST",
      url: "/dashboard/unit-test",
      icon: "clipboard",
    },

    {
      title: "PAPERS",
      url: "/dashboard/question-paper",
      icon: "newspaper",
    },
    {
      title: "CAREER",
      url: "/dashboard/career-notes",
      icon: "medal",
    },
    {
      title: "NOTES",
      url: "/dashboard/notes",
      icon: "browsers",
    },
    {
      title: "SAVED TEST",
      url: "/dashboard/view-test",
      icon: "copy",
    },
    // {
    //   title: "VIEW ALL QUESTIONS",
    //   url: "/dashboard/about-us",
    //   icon: "person-outline",
    // },
    {
      title: "BOOKMARKED",
      url: "/dashboard/bookmark",
      icon: "bookmark",
    },
    {
      title: "PERFORMANCE",
      url: "/dashboard/performance-graph",
      icon: "analytics",
    },
    // {
    //   title: "WEIGHTAGE",
    //   url: "",
    //   icon: "bar-chart",
    // },
    // {
    //   title: "NOTIFICATION",
    //   url: "",
    //   icon: "notifications",
    // },
    {
      title: "ABOUT US",
      url: "/dashboard/about-us",
      icon: "desktop",
    },
    {
      title: "SETTINGS",
      url: "/dashboard/settings",
      icon: "settings",
    },
    // {
    //   title: "SUPPORT",
    //   url: "/dashboard/about-us",
    //   icon: "person-outline",
    // },
    // {
    //   title: "REFRESH",
    //   url: "/dashboard/about-us",
    //   icon: "person-outline",
    // },
    {
      title: "LOGOUT",
      url: "",
      icon: "log-out",
    },
  ];

  selectedPath = "";

  testDetails: {
    testId: number;
    testDate: string;
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
    testSubmitted: string;
  }[] = [];

  userInfo: UserInfo = {
    firstName: "",
    lastName: "",
    contactNo: "",
    email: "",
    password: "",
    birthDate: "",
    gender: "",
    parentsContactNo: "",
    standard: "",
    state: "",
    district: "",
    taluka: "",
    appId: "",
    deviceId: "",
    distributerId: "",
    appKey: "",
    regiDate: "",
    expireDate: "",
    status: "",
    activated: "",
    imageData: "",
  };

  loaderTime;

  imageData: String = "";

  constructor(
    private http: HttpClient,
    private router: Router,
    private menuController: MenuController,
    private storage: Storage,
    private platform: Platform,
    private alertController: AlertController,
    private popoverController: PopoverController,
    private loadingController: LoadingController,
    private databaseService: DatabaseServiceService,
    private loadingServiceService: LoadingServiceService,
    private userInfoService: UserInfoService,
    public storageService: StorageServiceService,
    private internetServiceService: InternetServiceService,
    private alertServiceService: AlertServiceService,
    private apiService: ApiServiceService
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
    // console.log("dashboard page");
    let date = new Date().getDate().toString();
    let month = (new Date().getMonth() + 1).toString();
    let year = new Date().getFullYear().toString().substr(2);
    if (date.length == 1) date = "0" + date;
    if (month.length == 1) month = "0" + month;
    let today = year + month + date;
    // console.log("today", today);
    if (this.storageService.userLoggedIn == true) {
      this.databaseService.getDatabaseState().subscribe((ready) => {
        if (ready) {
          this.userInfoService.getUserInfo().then((result: UserInfo) => {
            this.userInfo = result;
            this.storageService.userInfo = result;
            if (parseInt(today) > parseInt(this.userInfo.expireDate)) {
              this.createAlert("Your subscription is expired.");
            } else {
              if (this.internetServiceService.networkConnected) {
                this.http
                  .post(
                    `${this.apiService.apiUrl}getActivationDetailsUpdated`,
                    {
                      email: this.userInfo.email,
                      distributerId: this.userInfo.distributerId,
                      appId: this.userInfo.appId,
                    }
                  )
                  .subscribe((data) => {
                    if (data == 1) {
                      this.createAlert(
                        "Your account is disabled. Contact to your distributer."
                      );
                    } else {
                      this.syncUserdata();
                    }
                  });
              }
            }
          });
        }
      });
    } else if (this.storageService.userLoggedIn == "demo") {
      this.storage.ready().then((ready) => {
        if (ready) {
          this.storage.get("expireDate").then((data) => {
            // console.log("expire", data);
            if (parseInt(today) > parseInt(data)) {
              this.createAlert("Your subscription for demo is expired.");
            } else {
              this.userInfoService.getUserInfo().then((result: UserInfo) => {
                this.userInfo = result;
                this.storageService.userInfo = result;
              });
            }
          });
        }
      });
    }
    // this.storage.get("imageData").then((data) => {
    //   this.imageData = data;
    //   this.storageService.imageData = data;
    // });
  }

  syncUserdata() {
    this.storage.get("dataSynced").then((data) => {
      if (!data) {
        this.http
          .post(`${this.apiService.apiUrl}fillUserDetailsUpdated`, this.userInfo)
          .subscribe(
            (data) => {
              if (data) {
                this.storage.set("dataSynced", true);
              }
            },
            (err) => this.storage.set("dataSynced", false)
          );
      }
    });
  }

  onClick(title) {
    if (title == "BOOKMARKED") {
      this.storage.set("page", "bookmark");
    } else if (title == "TEST") {
      this.storage.set("page", "unit-test");
    } else if (title == "PAPERS") {
      this.storage.set("page", "question-papers");
      this.storage.set("testTime", 0);
    } else if (title == "LOGOUT") {
      this.menuController.close();
      this.onLogout();
    }
  }

  onClose() {
    this.menuController.close();
  }

  onUnitTest() {
    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.set("page", "unit-test");
      }
    });
  }

  onQuestionPapers() {
    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.set("page", "question-papers");
        this.storage.set("testTime", 0);
      }
    });
  }

  onLogout() {
    this.menuController.toggle().then(async () => {
      const alert = await this.alertController.create({
        header: "Logout",
        subHeader: "Do you want to logout ?",
        cssClass: "alert-title",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
          },
          {
            text: "Logout",
            handler: () => {
              this.handleLogout();
            },
          },
        ],
      });
      await alert.present();
    });
  }

  handleLogout() {
    // console.log("in handle logout");
    // this.storage.set("userLoggedIn", false).then(_ => {
    //   this.storageService.userLoggedIn = false;
    //   this.router.navigateByUrl("/main")
    // })
    if (this.internetServiceService.networkConnected == false) {
      this.alertServiceService.createAlert(
        "Check your connection before proceeding to logout."
      );
    } else if (this.internetServiceService.networkConnected == true) {
      if (this.storageService.userLoggedIn == true) {
        this.loadingServiceService.createLoading("Loging Out");
        this.syncUserdata();
        this.storage.set("userLoggedIn", false);
        this.storage.set("dataSynced", false);
        this.storageService.registeration = false;
        this.storageService.userLoggedIn = false;
        this.http
          .post(`${this.apiService.apiUrl}logoutUpdated`, {
            email: this.userInfo.email,
            distributerId: this.userInfo.distributerId,
            appId: this.userInfo.appId,
          })
          .subscribe((data) => {
            if (data) {
              this.userInfoService.deleteFromUserInfo().then((_) => {
                this.loadingController
                  .dismiss()
                  .then((_) => this.router.navigateByUrl("/main"));
              });
            }
          });
      } else if (this.storageService.userLoggedIn == "demo") {
        this.storage.set("userLoggedIn", false);
        this.storageService.userLoggedIn = false;
        this.userInfoService
          .deleteFromUserInfo()
          .then((_) => this.router.navigateByUrl("/main"));
      }
    }
  }

  async createAlert(message) {
    const alert = await this.alertController.create({
      animated: true,
      subHeader: message,
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.handleLogout();
          },
        },
      ],
      backdropDismiss: false,
    });
    await alert.present();
  }
}
