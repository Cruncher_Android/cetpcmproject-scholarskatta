import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class ViewTestInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  inserIntoTable(userAnsArray, tableName) {
    // console.log('userAnsArray123', userAnsArray);
    let data1;
    userAnsArray.map(ans => {
    return this.databaseServiceService.getDataBase().executeSql(`INSERT INTO ${tableName} (TEST_ID, QUE_ID, USER_ANS) 
    values (?, ?, ?)`, [ans.testId, ans.queId, ans.userAns]).then(_ => {
      //  this.databaseServiceService.getDataBase().executeSql(`SELECT * from ${tableName}`, []).then(data => {
      //   data1 = data;
      //   //  console.log(data1)
      // })
    })
  })
  // console.log('array inserted');
  }

  updateTable(userAnsArray, tableName) {
    let data1;
    userAnsArray.map(ans => {
      return this.databaseServiceService.getDataBase().executeSql(`UPDATE ${tableName} SET USER_ANS = ? 
      WHERE QUE_ID = ? AND TEST_ID =  ?`, [ans.userAns, ans.queId, ans.testId]).then(_ => {
      //   this.databaseServiceService.getDataBase().executeSql(`SELECT * from ${tableName}`, []).then(data => {
      //    data1 = data;
      //     // console.log(data1)
      //  })
     })
    })
  }

  getUserAns(testId, tableName) {
    let userAnsArray = [];
    return this.databaseServiceService.getDataBase().executeSql(`SELECT * FROM ${tableName} WHERE TEST_ID = ?`, [testId]).then(data => {
      if(data.rows.length > 0) {
        for(let i = 0; i < data.rows.length; i++) {
          userAnsArray.push({
            queId: data.rows.item(i).QUE_ID,
            userAns: data.rows.item(i).USER_ANS
          })
        }
      }
      return userAnsArray;
    })
  }

  deleteFromTable() {
    this.databaseServiceService.getDataBase().executeSql(`DELETE FROM view_test_info`, []).then(_ => {
      // this.databaseServiceService.getDataBase().executeSql('SELECT * from view_test_info', []).then(data => {
      //   // console.log(data);
      // })
    })
  }
}
