import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionPaperPcbInfoService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
      this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS question_paper_pcb_info 
      (QUESTION_ID integer primary key autoincrement, QUESTION varchar(2000) NOT NULL, OPTIONA varchar(2000) NOT NULL, 
      OPTIONB varchar(2000) NOT NULL, OPTIONC varchar(2000) NOT NULL,OPTIOND varchar(2000) NOT NULL,  ANSWER varchar(1000) NULL, 
      HINT varchar(3000) NULL, QUESTION_LEVEL integer NOT NULL, SUBJECT_ID integer NOT NULL, CHAPTER_ID integer NOT NULL, 
      TOPIC_ID integer NOT NULL, ISQUESTIONASIMAGE integer NOT NULL, QUESTIONIMAGEPATH varchar(255) NULL, ISOPTIONASIMAGE integer NOT NULL,
      OPTIONIMAGEPATH varchar(255) NULL, ISHINTASIMAGE integer NOT NULL, HINTIMAGEPATH varchar(255) NULL, ATTEMPTEDVALUE integer NOT NULL, 
      QUESTION_TYPE integer NOT NULL, QUESTION_YEAR varchar(255) NULL)`, []);
      }
    });
  }
}
