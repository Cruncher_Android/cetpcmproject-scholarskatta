import { Injectable } from "@angular/core";
import { DatabaseServiceService } from "./database-service.service";
@Injectable({
  providedIn: "root",
})
export class TestLevelService {
  constructor(private databaseServiceService: DatabaseServiceService) {}

  getTest(tableName) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(`SELECT * from ${tableName}`, [])
      .then((data) => {
        let bestTest = {
          testId: data.rows.item(0).TEST_ID,
          obtainedMarks: data.rows.item(0).OBTAINED_MARKS,
          totalMarks: data.rows.item(0).TOTAL_MARKS,
          percentage: data.rows.item(0).PERCENTAGE,
          testType: data.rows.item(0).TEST_TYPE,
        };
        return bestTest;
      });
  }

  updateTest(test, tableName) {
    let obj = [
      test.testId,
      test.obtainedMarks,
      test.totalMarks,
      test.percentage,
      test.testType,
    ];

    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `update ${tableName} set TEST_ID = ?, OBTAINED_MARKS = ?, TOTAL_MARKS = ?, PERCENTAGE = ?, TEST_TYPE = ?`,
        obj
      )
      .then((_) => {
        // this.databaseServiceService
        //   .getDataBase()
        //   .executeSql(`SELECT * from ${tableName}`, [])
        //   .then((data) => {
        //     console.log("test in service", data);
        //   });
      });
  }
}
