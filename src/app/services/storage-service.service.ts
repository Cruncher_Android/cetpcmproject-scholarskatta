import { Injectable } from '@angular/core';
import { UserInfo } from './../main/main.page';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {

  questionCount: number = 0;
  registeration: boolean = false;
  userInfo: UserInfo = {
    firstName: "",
    lastName: "",
    contactNo: "",
    email: "",
    password: "",
    birthDate: "",
    gender: "",
    parentsContactNo: "",
    standard: "",
    state: "",
    district: "",
    taluka: "",
    deviceId: "",
    appKey: "",
    appId: "",
    distributerId: "",
    regiDate: "",
    expireDate: "",
    status: "",
    activated: "",
    imageData: "",
  };
  userLoggedIn: any;

  constructor() { }
}
