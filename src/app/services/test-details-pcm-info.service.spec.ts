import { TestBed } from '@angular/core/testing';

import { TestDetailsPcmInfoService } from './test-details-pcm-info.service';

describe('TestDetailsPcmInfoService', () => {
  let service: TestDetailsPcmInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestDetailsPcmInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
