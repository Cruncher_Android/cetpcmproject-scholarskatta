import { BehaviorSubject } from "rxjs";
import { DatabaseServiceService } from "./database-service.service";
import { Injectable } from "@angular/core";

export interface OverAllSummary {
  totalAttQue: number;
  totalCorrQue: number;
  phyTotalQue: number;
  phyAttQue: number;
  chemTotalQue: number;
  chemAttQue: number;
  mathsTotalQue: number;
  mathsAttQue: number;
}

@Injectable({
  providedIn: "root",
})
export class OverallSummaryService {
  overAllSummary = new BehaviorSubject(null);

  constructor(
    private databaseServiceService: DatabaseServiceService,
  ) {
    // this.databaseServiceService.getDatabaseState().subscribe((ready) => {
    //   if (ready) {
    //     databaseServiceService
    //       .getDataBase()
    //       .executeSql(
    //         `insert into overall_summary_demo values (0,0,0,0,0,0,0,0)`,
    //         []
    //       )
    //       .then((result) => {
    //         this.databaseServiceService
    //         .getDataBase()
    //         .executeSql(`SELECT * FROM overall_summary_demo`, [])
    //         .then((result) => {
    //           console.log("result of demo table", result);
    //         });
    //       });
    //   }
    // });
  }

  getOverallSummary(tableName) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(`SELECT * FROM ${tableName}`, [])
      .then((result) => {
        let overAllSummary: OverAllSummary;
        if (result.rows.length > 0) {
          // console.log("in service if");
          overAllSummary = {
            totalAttQue: result.rows.item(0).TOTAL_ATT_QUE,
            totalCorrQue: result.rows.item(0).TOTAL_CORR_QUE,
            phyTotalQue: result.rows.item(0).PHY_TOTAL_QUE,
            phyAttQue: result.rows.item(0).PHY_ATT_QUE,
            chemTotalQue: result.rows.item(0).CHEM_TOTAL_QUE,
            chemAttQue: result.rows.item(0).CHEM_ATT_QUE,
            mathsTotalQue: result.rows.item(0).MATHS_TOTAL_QUE,
            mathsAttQue: result.rows.item(0).MATHS_ATT_QUE,
          };
          this.overAllSummary.next(overAllSummary);
          // console.log('overall summary in service', this.overAllSummary)
          return overAllSummary;
        } else {
          // console.log("in service else");
          overAllSummary = {
            totalAttQue: 0,
            totalCorrQue: 0,
            phyTotalQue: 0,
            phyAttQue: 0,
            chemTotalQue: 0,
            chemAttQue: 0,
            mathsTotalQue: 0,
            mathsAttQue: 0,
          };
          this.overAllSummary.next(overAllSummary);
          return overAllSummary;
        }
      });
  }

  updateOverAllSummary(overAllSummary: OverAllSummary, tableName) {
    // console.log("overAllSummaryIn service", overAllSummary);
    // console.log("table name", tableName);
    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `UPDATE ${tableName} SET TOTAL_ATT_QUE = ?,  TOTAL_CORR_QUE = ?, PHY_TOTAL_QUE = ?, PHY_ATT_QUE = ?, CHEM_TOTAL_QUE = ?, 
        CHEM_ATT_QUE = ?, MATHS_TOTAL_QUE = ?, MATHS_ATT_QUE = ?`,
        [
          overAllSummary.totalAttQue,
          overAllSummary.totalCorrQue,
          overAllSummary.phyTotalQue,
          overAllSummary.phyAttQue,
          overAllSummary.chemTotalQue,
          overAllSummary.chemAttQue,
          overAllSummary.mathsTotalQue,
          overAllSummary.mathsAttQue,
        ]
      )
      .then((result) => {
        // console.log("result in update demo table", result);
        // this.databaseServiceService
        //   .getDataBase()
        //   .executeSql(`SELECT * FROM ${tableName}`, [])
        //   .then((result) => {
        //     console.log("result of demo table", result);
        //   });
        this.getOverallSummary(tableName);
      });
  }

  observeOverallSummary(tableName) {
    // console.log("tablename", tableName);
    this.getOverallSummary(tableName);
    return this.overAllSummary.asObservable();
  }
}
